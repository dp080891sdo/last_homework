package com.sviatukhov.homework.dtos;

import lombok.Data;

@Data
public class PhoneDTO {
    private int vendorCode;
    private String brand;
    private String model;
    private int memory;
    private double price;
}
