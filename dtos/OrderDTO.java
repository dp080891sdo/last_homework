package com.sviatukhov.homework.dtos;

import com.sviatukhov.homework.enums.OrderStatus;
import lombok.Data;

@Data
public class OrderDTO {
    private int phoneId;
    private int userId;
    private OrderStatus status;

    public OrderDTO(int phoneId, int userId) {
        this.phoneId = phoneId;
        this.userId = userId;
        this.status = OrderStatus.CREATED;
    }
}
