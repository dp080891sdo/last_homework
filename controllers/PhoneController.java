package com.sviatukhov.homework.controllers;

import com.sviatukhov.homework.dtos.PhoneDTO;
import com.sviatukhov.homework.entities.PhoneEntity;
import com.sviatukhov.homework.services.PhoneService;
import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Data
@RestController
@RequestMapping("/phone")
public class PhoneController {
    PhoneService phoneService;

    public PhoneController(PhoneService phoneService) {
        this.phoneService = phoneService;
    }

    @PostMapping("/create")
    public void createPhone(@RequestBody PhoneDTO dto) {
        phoneService.createPhone(dto);
    }

    @PostMapping("/delete")
    public void deletePhone(@RequestBody PhoneDTO dto) {
        phoneService.deletePhone(dto);
    }

    @GetMapping("/getAll")
    public List<PhoneEntity> getAllPhones() {
        return phoneService.getAllPhones();
    }

    @GetMapping("/getAll")
    public PhoneEntity getPhone(String brand, String model) {
        return phoneService.getPhone(brand, model);
    }

    @GetMapping("/getFirstPage")
    public Page<PhoneEntity> getFirstPage(Integer pageNumber, Integer countElements) {
        return phoneService.getPage(pageNumber, countElements);
    }
}
