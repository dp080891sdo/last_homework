package com.sviatukhov.homework.controllers;

import com.sviatukhov.homework.dtos.OrderDTO;
import com.sviatukhov.homework.entities.OrderEntity;
import com.sviatukhov.homework.services.OrderService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
public class OrderController {

    OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("/create")
    public void createOrder(@RequestBody OrderDTO dto) {
        orderService.createOrder(dto);
    }

    @PostMapping("/update")
    public void updateOrderStatus(@RequestBody OrderEntity orderEntity) {
        orderService.confirmOrder(orderEntity);
    }

}
