package com.sviatukhov.homework.controllers;

import com.sviatukhov.homework.dtos.UserDTO;
import com.sviatukhov.homework.entities.UserEntity;
import com.sviatukhov.homework.services.UserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {
    UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/create")
    public void createUser(@RequestBody UserDTO dto) {
        userService.createUser(dto);
    }

    @PostMapping("/update")
    public void updateRole(@RequestBody UserEntity userEntity) {
        userService.setRoleForUser(userEntity);
    }
}
