package com.sviatukhov.homework.repositories;

import com.sviatukhov.homework.entities.PhoneEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PhoneRepository extends JpaRepository<PhoneEntity, Integer> {
    void deleteAllByVendorCode(Integer vendorCode);

    PhoneEntity findByBrandAndModel(String brand, String model);
}
