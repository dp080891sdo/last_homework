package com.sviatukhov.homework.repositories;

import com.sviatukhov.homework.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Integer> {
    List<UserEntity> findAllByFirstNameAndLastNameAndPhoneNumber(String firstName, String lastName, String phoneNumber);

    UserEntity findByFirstNameAndLastNameAndPhoneNumber(String firstName, String lastName, String phoneNumber);
}
