package com.sviatukhov.homework.repositories;

import com.sviatukhov.homework.entities.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<OrderEntity, Integer> {
    OrderEntity findFirstBy();

    List<OrderEntity> findAllByUserIdAndStatus(Integer id, String status);

    List<OrderEntity> findAllByOrderNumber(Integer orderNumber);
}
