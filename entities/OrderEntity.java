package com.sviatukhov.homework.entities;

import com.sviatukhov.homework.enums.OrderStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Data
@Table(name = "orders")
public class OrderEntity {
    @Id
    @SequenceGenerator(name = "orders_id_seq", sequenceName = "orders_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "orders_id_seq")
    @Column(name = "id", updatable = false)
    private Integer id;
    @Column(name = "order_number")
    private Integer orderNumber;
    @Column(name = "product_id")
    private Integer productId;
    @Column(name = "user_id")
    private int userId;
    @Column(name = "status")
    private String status;

    public OrderEntity(Integer id, Integer productId, int userId) {
        this.orderNumber = id;
        this.productId = productId;
        this.userId = userId;
        this.status = OrderStatus.CREATED.toString();
    }
}
