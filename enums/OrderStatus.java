package com.sviatukhov.homework.enums;

public enum OrderStatus {
    CREATED, CONFIRMED
}
