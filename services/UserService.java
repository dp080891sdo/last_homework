package com.sviatukhov.homework.services;

import com.sviatukhov.homework.dtos.UserDTO;
import com.sviatukhov.homework.entities.UserEntity;
import com.sviatukhov.homework.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void createUser(UserDTO dto) {
        List<UserEntity> list = userRepository
                .findAllByFirstNameAndLastNameAndPhoneNumber(
                        dto.getFirstName(),
                        dto.getLastName(),
                        dto.getPhoneNumber());
        if (list.isEmpty()) {
            UserEntity user = new UserEntity(
                    dto.getFirstName(),
                    dto.getLastName(),
                    dto.getPhoneNumber(),
                    dto.getIsAdmin());
            userRepository.save(user);
        } else {
            System.out.println("User is exists");
        }
    }

    public void setRoleForUser(UserEntity userEntity) {
        List<UserEntity> list = userRepository
                .findAllByFirstNameAndLastNameAndPhoneNumber(
                        userEntity.getFirstName(),
                        userEntity.getLastName(),
                        userEntity.getPhoneNumber());
        if (!list.isEmpty()) {
            list.forEach(userEntity1 -> userEntity1.setIsAdmin(userEntity.getIsAdmin()));
            userRepository.saveAll(list);
        }
    }
}
