package com.sviatukhov.homework.services;

import com.sviatukhov.homework.dtos.OrderDTO;
import com.sviatukhov.homework.entities.OrderEntity;
import com.sviatukhov.homework.enums.OrderStatus;
import com.sviatukhov.homework.repositories.OrderRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {
    private static Integer ORDER_NUMBER = 1;
    private final OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public void createOrder(OrderDTO dto) {
        OrderEntity orderEntity = orderRepository.findFirstBy();
        List<OrderEntity> list = orderRepository
                .findAllByUserIdAndStatus(dto.getUserId(), OrderStatus.CREATED.toString());
        OrderEntity order;
        if (orderEntity == null || list.isEmpty()) {
            order = new OrderEntity(
                    ORDER_NUMBER++,
                    dto.getPhoneId(),
                    dto.getUserId());
        } else {
            order = new OrderEntity(
                    list.get(0).getOrderNumber(),
                    dto.getPhoneId(),
                    dto.getUserId());
        }
        orderRepository.save(order);
    }

    public void confirmOrder(OrderEntity orderEntity) {
        List<OrderEntity> list = orderRepository.findAllByOrderNumber(orderEntity.getOrderNumber());
        if (!list.isEmpty()) {
            list.forEach((entity) -> entity
                    .setStatus(OrderStatus.CONFIRMED.toString()));
            orderRepository.saveAll(list);
        }
    }
}
