package com.sviatukhov.homework.services;

import com.sviatukhov.homework.dtos.PhoneDTO;
import com.sviatukhov.homework.entities.PhoneEntity;
import com.sviatukhov.homework.repositories.PhoneRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhoneService {
    private final PhoneRepository phoneRepository;

    public PhoneService(PhoneRepository phoneRepository) {
        this.phoneRepository = phoneRepository;
    }

    public void createPhone(PhoneDTO dto) {
        PhoneEntity phone = new PhoneEntity(
                dto.getVendorCode(),
                dto.getBrand(),
                dto.getModel(),
                dto.getMemory(),
                dto.getPrice());
        phoneRepository.save(phone);
    }

    public void deletePhone(PhoneDTO dto) {
        phoneRepository.deleteAllByVendorCode(dto.getVendorCode());
    }

    public List<PhoneEntity> getAllPhones() {
        return phoneRepository.findAll();
    }

    public PhoneEntity getPhone(String brand, String model) {
        return phoneRepository.findByBrandAndModel(brand, model);
    }

    public Page<PhoneEntity> getPage(Integer pageNumber, Integer countPhones) {
        PageRequest page = PageRequest.of(pageNumber, countPhones);
        return phoneRepository.findAll(page);
    }
}
